<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/psr-traits package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\PsrTraits;

use Psr\Http\Message\ResponseInterface;

/**
 * Trait PsrHttpResponseTrait.
 *
 * Useful methods for interacting with a \Psr\Http\Message\ResponseInterface.
 */
trait PsrHttpResponseTrait
{
    /**
     * Determine if the request failed by looking at the statusCode from the response.
     *
     * @param ResponseInterface $response The response to look at
     *
     * @return bool Whether the request failed
     */
    final protected function responseHasErrorStatus(ResponseInterface $response): bool
    {
        $status = $response->getStatusCode();

        return $status < 200 || $status > 399;
    }
}
