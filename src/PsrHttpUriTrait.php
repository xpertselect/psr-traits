<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/psr-traits package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\PsrTraits;

use Psr\Http\Message\UriInterface;

/**
 * Trait PsrHttpResponseTrait.
 *
 * Useful methods for interacting with a \Psr\Http\Message\UriInterface.
 */
trait PsrHttpUriTrait
{
    /**
     * Retrieve the complete path (path + querystring) from a given URI.
     *
     * @param UriInterface $uri The URI to extract the data from
     *
     * @return string The complete path
     */
    final protected function getPathFromUri(UriInterface $uri): string
    {
        $path = $uri->getPath();

        if (!empty($queryString = $uri->getQuery())) {
            $path .= '?' . $queryString;
        }

        return $path;
    }
}
