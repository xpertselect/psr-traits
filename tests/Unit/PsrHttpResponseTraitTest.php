<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/psr-traits package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Mockery;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;
use XpertSelect\PsrTraits\PsrHttpResponseTrait;

/**
 * @internal
 */
final class PsrHttpResponseTraitTest extends TestCase
{
    private $mock;

    public function setUp(): void
    {
        $this->mock = new class () {
            use PsrHttpResponseTrait;

            public function testHasErrorStatus(ResponseInterface $response): bool
            {
                return $this->responseHasErrorStatus($response);
            }
        };
    }

    /**
     * @dataProvider statusCodeDataset
     */
    public function testIsErrorForStatusCodes(int $statusCode, bool $isError): void
    {
        $response = Mockery::mock(ResponseInterface::class);
        $response->expects('getStatusCode')->andReturn($statusCode);

        $this->assertEquals($isError, $this->mock->testHasErrorStatus($response));
    }

    public function statusCodeDataset(): array
    {
        return [
            [199, true],
            [200, false],
            [399, false],
            [400, true],
        ];
    }
}
