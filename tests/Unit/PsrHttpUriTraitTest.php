<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/psr-traits package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Mockery;
use Psr\Http\Message\UriInterface;
use Tests\TestCase;
use XpertSelect\PsrTraits\PsrHttpUriTrait;

/**
 * @internal
 */
final class PsrHttpUriTraitTest extends TestCase
{
    private $mock;

    public function setUp(): void
    {
        $this->mock = new class () {
            use PsrHttpUriTrait;

            public function testGetPath(UriInterface $uri): string
            {
                return $this->getPathFromUri($uri);
            }
        };
    }

    public function testUriWithoutQueryJustReturnsPath(): void
    {
        $response = Mockery::mock(UriInterface::class);
        $response->expects('getPath')->andReturn('/foo');
        $response->expects('getQuery')->andReturn('');

        $this->assertEquals('/foo', $this->mock->testGetPath($response));
    }

    public function testUriWithoutPathReturnsQuery(): void
    {
        $response = Mockery::mock(UriInterface::class);
        $response->expects('getPath')->andReturn('');
        $response->expects('getQuery')->andReturn('foo=bar');

        $this->assertEquals('?foo=bar', $this->mock->testGetPath($response));
    }

    public function testUriWithPathAndQueryReturnsEntireURI(): void
    {
        $response = Mockery::mock(UriInterface::class);
        $response->expects('getPath')->andReturn('/baz');
        $response->expects('getQuery')->andReturn('foo=bar');

        $this->assertEquals('/baz?foo=bar', $this->mock->testGetPath($response));
    }
}
