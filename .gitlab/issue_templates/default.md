## Summary

Summarize the bug encountered concisely.

## Version

What version of `xpertselect/psr-traits` are you using?

```shell
composer show xpertselect/psr-traits 2>&1 | grep versions
```
