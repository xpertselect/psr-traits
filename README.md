# XpertSelect / PSR Traits

[gitlab.com/xpertselect/psr-traits](https://gitlab.com/xpertselect/psr-traits)

XpertSelect package providing utilities for interacting with PSR requests and responses.

## License

View the `LICENSE.md` file for licensing details.

## Installation

Installation of [`xpertselect/psr-traits`](https://packagist.org/packages/xpertselect/psr-traits) is done via [Composer](https://getcomposer.org).

```shell
composer require xpertselect/psr-traits
```
